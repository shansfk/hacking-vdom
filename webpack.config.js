var webpack = require('webpack');

const path = require('path');

module.exports = {
  entry: {
    preact: './preact.index.js',
    play: './play.index.js'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json', '.less'],
    alias: {
      'preact': path.resolve(__dirname, './preact/preact'),
      'mithril': path.resolve(__dirname, './mithril/index'),
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery"
    }),
    new webpack.ProvidePlugin({
        'preact': 'preact'
    }),
    new webpack.ProvidePlugin({
        'mithril': 'mithril'
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3000,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules|dist/,
        loader: "babel-loader"
      }
    ]
  },
  devtool: '#inline-source-map'
};
